def Acomodar1(x):
    NLista=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

    MAX=max(x)
    NLista.pop(8)
    NLista.insert(8,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(7)
    NLista.insert(7,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(9)
    NLista.insert(9,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(6)
    NLista.insert(6,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(10)
    NLista.insert(10,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(5)
    NLista.insert(5,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(11)
    NLista.insert(11,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(4)
    NLista.insert(4,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(12)
    NLista.insert(12,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(3)
    NLista.insert(3,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(13)
    NLista.insert(13,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(2)
    NLista.insert(2,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(14)
    NLista.insert(14,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(1)
    NLista.insert(1,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(15)
    NLista.insert(15,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(0)
    NLista.insert(0,MAX)
    x.remove(MAX)



    return NLista

def Acomodar2(x):
    NLista=[0,0,0,0]

    MAX=max(x)
    NLista.pop(0)
    NLista.insert(0,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(3)
    NLista.insert(3,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(1)
    NLista.insert(1,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(2)
    NLista.insert(2,MAX)
    x.remove(MAX)
    return NLista

def Acomodar3(x):
    NLista=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

    MAX=max(x)
    NLista.pop(7)
    NLista.insert(7,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(6)
    NLista.insert(6,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(8)
    NLista.insert(8,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(5)
    NLista.insert(5,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(9)
    NLista.insert(9,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(4)
    NLista.insert(4,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(10)
    NLista.insert(10,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(3)
    NLista.insert(3,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(11)
    NLista.insert(11,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(2)
    NLista.insert(2,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(12)
    NLista.insert(12,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(1)
    NLista.insert(1,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(13)
    NLista.insert(13,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(0)
    NLista.insert(0,MAX)
    x.remove(MAX)

    MAX=max(x)
    NLista.pop(14)
    NLista.insert(14,MAX)
    x.remove(MAX)

    return NLista
