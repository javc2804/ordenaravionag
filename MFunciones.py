def Poblacion():
    import random
    x=[]
    for i in range (16):
        b=random.uniform(1,3.5)
        a=round(b,2)
        x.append(a)
    return x

def Capacidad(x,y=[]):
    if(x>37.5):
        while(x>37.5):
            y=Poblacion()
            z=y
            x=sum(y)
    else:
        z=y
    return z

def Poblacion1():
    import random
    x=[]
    for i in range (4):
        b=random.uniform(1,3.5)
        a=round(b,2)
        x.append(a)
    return x

def Capacidad1(x,y=[]):
    if(x>7):
        while(x>7):
            y=Poblacion1()
            z=y
            x=sum(y)
    else:
        z=y
    return z

def Poblacion2():
    import random
    x=[]
    for i in range (15):
        b=random.uniform(1,2.7)
        a=round(b,2)
        x.append(a)
    return x

def Capacidad2(x,y=[]):
    if(x>20):
        while(x>20):
            y=Poblacion2()
            z=y
            x=sum(y)
    else:
        z=y
    return z

#def PosiblesSoluciones(x):
    #import itertools
    #c=[]
    #c=list(itertools.permutations(x))
    #return c
