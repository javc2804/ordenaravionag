import random
import MFunciones
import MFunciones2

from tkinter import *

Container=[]
Cromosoma=[]
PesoT=0
Capacidad=0
lista=[]
Izq=[]
Der=[]
A1=[]
A2=[]

Container1=[]
Cromosoma1=[]
PesoT1=0
Capacidad1=0
lista1=[]
Izq1=[]
Der1=[]
A11=[]
A21=[]

Container2=[]
Cromosoma2=[]
PesoT2=0
Capacidad2=0
lista2=[]
Izq2=[]
Der2=[]
A12=[]
A22=[]

root = Tk()
frame = Frame(root)
frame.pack()
root.geometry("1100x1100+0+0")
bottomframe = Frame(root)
bottomframe.pack( side = LEFT )


lbl=Label(root,text="Boeing 747-400ERF (Realizado por: Javier Villarroel)",fg="black",font=("Radio Space Bold Italic",25)).place(x=300,y=550)


for i in range(2):
    Container=(MFunciones.Poblacion())
    PesoT=sum(Container)
    Cromosoma=MFunciones.Capacidad(PesoT,Container)
    #Cromosoma=MFunciones.PosiblesSoluciones(Container)
    #print(Cromosoma)
    lista.append(Cromosoma)

    Container1=(MFunciones.Poblacion1())
    PesoT1=sum(Container1)
    Cromosoma1=MFunciones.Capacidad1(PesoT1,Container1)
    lista1.append(Cromosoma1)

    Container2=(MFunciones.Poblacion2())
    PesoT2=sum(Container2)
    Cromosoma2=MFunciones.Capacidad2(PesoT2,Container2)
    lista2.append(Cromosoma2)
i=0
j=0
SUMA=0
SUMA=round(sum(lista[0]),2)
ACUMULADOR=0
ACUMULADOR+=SUMA
button=Button(frame, text="Derecha",bg="red")
button.grid(column=0, row=1)

for i in lista[0]:
    j+=1
    button=Button(frame, text=i)
    button.grid(column=j, row=1)

button=Button(frame, text="suma")
button.grid(column=17, row=1)

button=Button(frame, text=SUMA)
button.grid(column=18, row=1)

i=0
j=0
SUMA=0
SUMA=round(sum(lista[1]),2)
ACUMULADOR+=SUMA
button=Button(frame, text="Izquierda",bg="red")
button.grid(column=0, row=2)

for i in lista[1]:
    j+=1
    button=Button(frame, text=i)
    button.grid(column=j, row=2)
button=Button(frame, text="suma")
button.grid(column=17, row=2)

button=Button(frame, text=SUMA)
button.grid(column=18, row=2)

i=0
j=0
for i in range(18):
    j+=1
    button=Button(frame, text="        ",bg="black")
    button.grid(column=j, row=3)

Izq=lista[0]
Der=lista[1]
A1=MFunciones2.Acomodar1(Izq)
A2=MFunciones2.Acomodar1(Der)
MAXA1=0
MAXA2=0
MAXA1=round(sum(A1),2)
MAXA2=round(sum(A2),2)

lbl=Label(root,text="Acomodado(HIJO) Containers tipo LD-1 ",fg="black").place(x=600,y=63)
button=Button(frame, text="Derecha",fg="white",bg="blue")
button.grid(column=0, row=5)

j=0
for i in A1:
    j+=1
    if(j>=6 and j<=11):
        button=Button(frame, text=i,bg="red")
        button.grid(column=j, row=5)

    elif(j>=4 and j<=5 or j>=12 and j<=13):
        button=Button(frame, text=i,bg="orange")
        button.grid(column=j, row=5)

    elif(j>=2 and j<=3 or j>=14 and j<=15):
        button=Button(frame, text=i,bg="yellow")
        button.grid(column=j, row=5)

    else:
        button=Button(frame, text=i,bg="green")
        button.grid(column=j, row=5)

button=Button(frame, text="suma")
button.grid(column=17, row=5)


button=Button(frame, text=MAXA1)
button.grid(column=18, row=5)

button=Button(frame, text="Izquierda",fg="white",bg="blue")
button.grid(column=0, row=6)

j=0
for i in A2:
    j+=1
    if(j>=6 and j<=11):
        button=Button(frame, text=i,bg="red")
        button.grid(column=j, row=6)

    elif(j>=4 and j<=5 or j>=12 and j<=13):
        button=Button(frame, text=i,bg="orange")
        button.grid(column=j, row=6)

    elif(j>=2 and j<=3 or j>=14 and j<=15):
        button=Button(frame, text=i,bg="yellow")
        button.grid(column=j, row=6)

    else:
        button=Button(frame, text=i,bg="green")
        button.grid(column=j, row=6)
button=Button(frame, text="suma")
button.grid(column=17, row=6)


button=Button(frame, text=MAXA2)
button.grid(column=18, row=6)
j=0
i=0
for i in range(18):
    j+=1
    button=Button(frame, text="        ",bg="black")
    button.grid(column=j, row=7)

lbl=Label(root,text="Cromosoma: Proa,popa, desordenado(Padres)  Containers tipo LD-1 Y/O LD-2",fg="black").place(x=500,y=145)

button=Button(frame, text="Proa",bg="red")
button.grid(column=0, row=8)

i=0
j=0
for i in lista1[0]:
    j+=1
    button=Button(frame, text=i)
    button.grid(column=j, row=8)

Izq1=lista1[0]
Der1=lista1[1]
MAXA11=0
MAXA21=0
MAXA11=round(sum(Izq1),2)
MAXA21=round(sum(Der1),2)
ACUMULADOR+=MAXA11
ACUMULADOR+=MAXA21

button=Button(frame, text="suma")
button.grid(column=17, row=8)

button=Button(frame, text=MAXA11)
button.grid(column=18, row=8)


button=Button(frame, text="popa",bg="red")
button.grid(column=0, row=9)

i=0
j=0
for i in lista1[1]:
    j+=1
    button=Button(frame, text=i)
    button.grid(column=j, row=9)

button=Button(frame, text="suma")
button.grid(column=17, row=9)

button=Button(frame, text=MAXA21)
button.grid(column=18, row=9)

i=0
j=0
for i in range(18):
    j+=1
    button=Button(frame, text="        ",bg="black")
    button.grid(column=j, row=10)

A11=MFunciones2.Acomodar2(Izq1)
A21=MFunciones2.Acomodar2(Der1)

lbl=Label(root,text="Acomodado (Hijos)",fg="black").place(x=650,y=230)

button=Button(frame, text="Proa",fg="white",bg="blue")
button.grid(column=0, row=11)

i=0
j=0
for i in A11:
    j+=1
    if(j==1 or j==4):
        button=Button(frame, text=i,bg="red")
        button.grid(column=j, row=11)
    else:
        button=Button(frame, text=i,bg="orange")
        button.grid(column=j, row=11)

button=Button(frame, text="suma")
button.grid(column=17, row=11)


button=Button(frame, text=MAXA11)
button.grid(column=18, row=11)

button=Button(frame, text="popa",fg="white",bg="blue")
button.grid(column=0, row=12)
i=0
j=0
for i in A21:
    j+=1
    if(j==1 or j==4):
        button=Button(frame, text=i,bg="red")
        button.grid(column=j, row=12)
    else:
        button=Button(frame, text=i,bg="orange")
        button.grid(column=j, row=12)

button=Button(frame, text="suma")
button.grid(column=17, row=12)


button=Button(frame, text=MAXA21)
button.grid(column=18, row=12)

i=0
j=0
for i in range(18):
    j+=1
    button=Button(frame, text="        ",bg="black")
    button.grid(column=j, row=13)

lbl=Label(root,text="Cromosoma: Parte de abajo desordenado(Padres)  Containers tipo LD-2 ",fg="black").place(x=500,y=315)


button=Button(frame, text="Derecha",bg="red")
button.grid(column=0, row=14)

SUMA=0
i=0
j=0
SUMA=round(sum(lista2[0]),2)
ACUMULADOR+=SUMA
for i in lista2[0]:
    j+=1
    button=Button(frame, text=i)
    button.grid(column=j, row=14)

button=Button(frame, text="suma")
button.grid(column=17, row=14)

button=Button(frame, text=SUMA)
button.grid(column=18, row=14)

i=0
j=0
SUMA=0
SUMA=round(sum(lista2[1]),2)
ACUMULADOR+=SUMA
button=Button(frame, text="Izquierda",bg="red")
button.grid(column=0, row=15)

for i in lista2[1]:
    j+=1
    button=Button(frame, text=i)
    button.grid(column=j, row=15)
button=Button(frame, text="suma")
button.grid(column=17, row=15)

button=Button(frame, text=SUMA)
button.grid(column=18, row=15)

i=0
j=0
for i in range(18):
    j+=1
    button=Button(frame, text="        ",bg="black")
    button.grid(column=j, row=16)

Izq2=lista2[0]
Der2=lista2[1]
A12=MFunciones2.Acomodar3(Izq2)
A22=MFunciones2.Acomodar3(Der2)
MAXA12=0
MAXA22=0
MAXA12=round(sum(A12),2)
MAXA22=round(sum(A22),2)

lbl=Label(root,text="Acomodado(HIJO) Containers tipo LD-1 ",fg="black").place(x=600,y=400)
button=Button(frame, text="Derecha",fg="white",bg="blue")
button.grid(column=0, row=17)

j=0
for i in A12:
    j+=1
    if(j>=6 and j<=10):
        button=Button(frame, text=i,bg="red")
        button.grid(column=j, row=17)

    elif(j>=4 and j<=5 or j>=11 and j<=12):
        button=Button(frame, text=i,bg="orange")
        button.grid(column=j, row=17)

    elif(j>=2 and j<=3 or j>=13 and j<=14):
        button=Button(frame, text=i,bg="yellow")
        button.grid(column=j, row=17)

    else:
        button=Button(frame, text=i,bg="green")
        button.grid(column=j, row=17)

button=Button(frame, text="suma")
button.grid(column=17, row=17)


button=Button(frame, text=MAXA12)
button.grid(column=18, row=17)

button=Button(frame, text="Izquierda",fg="white",bg="blue")
button.grid(column=0, row=18)

j=0
for i in A22:
    j+=1
    if(j>=6 and j<=10):
        button=Button(frame, text=i,bg="red")
        button.grid(column=j, row=18)

    elif(j>=4 and j<=5 or j>=11 and j<=12):
        button=Button(frame, text=i,bg="orange")
        button.grid(column=j, row=18)

    elif(j>=2 and j<=3 or j>=13 and j<=14):
        button=Button(frame, text=i,bg="yellow")
        button.grid(column=j, row=18)

    else:
        button=Button(frame, text=i,bg="green")
        button.grid(column=j, row=18)

button=Button(frame, text="suma")
button.grid(column=17, row=18)


button=Button(frame, text=MAXA22)
button.grid(column=18, row=18)
TOTAL=0
TOTAL=round(ACUMULADOR,2)

button=Button(frame, text=TOTAL)
button.grid(column=18, row=19)

button=Button(frame, text="Peso total")
button.grid(column=17, row=19)

root.mainloop()
